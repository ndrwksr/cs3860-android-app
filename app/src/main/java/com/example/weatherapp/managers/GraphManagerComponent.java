package com.example.weatherapp.managers;

import dagger.Component;

@Component(modules = {GraphManagerModule.class})
public interface GraphManagerComponent {
    GraphManager getGraphManager();
}
