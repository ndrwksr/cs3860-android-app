package com.example.weatherapp.managers;

import android.icu.util.GregorianCalendar;
import android.util.Pair;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class WeatherItem {
    private static final DateFormat ISO_8601_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);

    private final String year;
    private final String month;
    private final String day;
    private final String hour;
    private final String value;

    public WeatherItem(String year, String month, String day, String hour, String value) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.value = value;
    }

    Pair<String, Double> convertToDataPoint() {
        try {
            final int year = Integer.parseInt(getYear());
            final int month = Integer.parseInt(getMonth());
            final int day = Integer.parseInt(getDay());
            final int hour = Integer.parseInt(getHour());
            final Double value = Double.parseDouble(getValue());

            final Date timestamp = new GregorianCalendar(
                    year,
                    month - 1,  //Month is zero-indexed by Calendar,
                    day,               //but not by SQL server
                    hour,
                    0
            ).getTime();

            return new Pair<>(ISO_8601_FORMAT.format(timestamp), value);
        } catch (NullPointerException | NumberFormatException e) {
            return null;
        }
    }


    private String getYear() {
        return year;
    }

    private String getMonth() {
        return month;
    }

    private String getDay() {
        return day;
    }

    private String getHour() {
        return hour;
    }

    private String getValue() {
        return value;
    }
}
