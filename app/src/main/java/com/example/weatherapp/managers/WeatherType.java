package com.example.weatherapp.managers;

public enum WeatherType {
    RAINFALL("rainfall", null),
    WIND_SPEED("windspeed", "wind"),
    HUMIDITY("humidity", null),
    TEMPERATURE("temperature", "temp");

    private final String apiResourceString;
    private final String thresholdString;

    WeatherType(String apiResourceString, String thresholdString) {
        this.apiResourceString = apiResourceString;
        this.thresholdString = thresholdString;
    }

    public String getApiResourceString() {
        return apiResourceString;
    }

    public String getThresholdString() {
        return thresholdString;
    }
}
