package com.example.weatherapp.managers;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Component(modules = {NetModule.class})
public interface NetComponent {
    OkHttpClient okHttpClient();

    Retrofit retrofit();

    Gson gson();
}