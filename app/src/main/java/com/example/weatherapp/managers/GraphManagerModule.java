package com.example.weatherapp.managers;

import dagger.Module;
import dagger.Provides;

@Module
public class GraphManagerModule {
    @Provides
    GraphManager getGraphManager() {
        return new GraphManagerImpl();
    }
}
