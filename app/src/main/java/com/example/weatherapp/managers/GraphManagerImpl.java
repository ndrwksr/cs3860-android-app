package com.example.weatherapp.managers;

import android.util.Log;
import android.util.Pair;

import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.inject.Inject;

public class GraphManagerImpl implements GraphManager {
    private static final long UNIX_TIME_2019 = 1546300800;
    private static final DateFormat ISO_8601_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private static final DateFormat DAY_DATE_FORMAT =
            new SimpleDateFormat("HH:mm", Locale.US);
    private static final DateFormat WEEK_DATE_FORMAT =
            new SimpleDateFormat("EEE", Locale.US);
    private static final DateFormat MONTH_DATE_FORMAT =
            new SimpleDateFormat("MMM dd", Locale.US);
    private static final long UNIX_TIME_SCALAR = 1000L;
    private static final long SMALL_UNIX_TIME_SCALAR = 100L;

    @Inject
    public WeatherService weatherService;

    @Inject
    public GraphManagerImpl() {
    }

    @Override
    public BarDataSet getBarDataSet(
            final List<Pair<String, Double>> rawData,
            final String label
    ) throws IllegalArgumentException {
        if (rawData.isEmpty()) {
            return null;
        }
        final List<Pair<String, Double>> sortedData = sortRawData(rawData);

        ArrayList<Long> smallUnixTimes = getSmallUnixTimes(sortedData);
        if (smallUnixTimes.isEmpty()) {
            return null;
        }

        ArrayList<BarEntry> barEntries = new ArrayList<>();

        for (int i = 0; i < sortedData.size(); i++) {
            barEntries.add(new BarEntry(
                    (float) smallUnixTimes.get(i),
                    (float) (double)/*unbox*/ sortedData.get(i).second)
            );
        }

        return new BarDataSet(barEntries, label);
    }

    private List<Pair<String, Double>> sortRawData(List<Pair<String, Double>> rawData) {
        return rawData.stream().sorted((pair1, pair2) -> {
            Date date1 = null;
            Date date2 = null;

            try {
                date1 = ISO_8601_FORMAT.parse(pair1.first);
                date2 = ISO_8601_FORMAT.parse(pair2.first);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date1 != null && date2 != null ?
                    date1.compareTo(date2) :
                    0;
        }).collect(Collectors.toList());
    }

    @Override
    public LineDataSet getLineDataSet(
            final List<Pair<String, Double>> rawData,
            final String label
    ) {
        final List<Pair<String, Double>> sortedData = sortRawData(rawData);
        ArrayList<Long> smallUnixTimes = getSmallUnixTimes(sortedData);

        ArrayList<Entry> lineEntries = new ArrayList<>();

        for (int i = 0; i < sortedData.size(); i++) {
            lineEntries.add(new BarEntry(
                    (float) smallUnixTimes.get(i),
                    (float) (double)/*unbox*/ sortedData.get(i).second)
            );
        }

        return new LineDataSet(lineEntries, label);
    }

    private ArrayList<Long> getSmallUnixTimes(final List<Pair<String, Double>> rawData) {
        if (rawData == null || rawData.size() == 0) {
            throw new IllegalArgumentException("rawData cannot be null nor empty.");
        }
        ArrayList<Long> unixTimes = new ArrayList<>();

        rawData.forEach(pair -> {
            Date date;
            if (pair == null || pair.first == null) {
                return;
            }
            try {
                date = ISO_8601_FORMAT.parse(pair.first);
            } catch (ParseException e) {
                Log.e("PARSE", "Failed to parse ISO8601 date: " + pair.first, e);
                return;
            }

            long unixTime = date.getTime();
            unixTimes.add(unixTime);
        });

        ArrayList<Long> smallUnixTimes = new ArrayList<>();
        unixTimes.forEach(unixTime -> {
            long scaledUnixTime = (unixTime / UNIX_TIME_SCALAR);
            long smallUnixTime = (scaledUnixTime - UNIX_TIME_2019) / SMALL_UNIX_TIME_SCALAR;
            smallUnixTimes.add(smallUnixTime);
        });

        return smallUnixTimes;
    }

    @Override
    public ValueFormatter getValueFormatter(TimeScale timeScale) {
        switch (timeScale) {
            case ONE_DAY:
                return new DateValueFormatter(DAY_DATE_FORMAT);
            case ONE_WEEK:
                return new DateValueFormatter(WEEK_DATE_FORMAT);
            case ONE_MONTH:
                return new DateValueFormatter(MONTH_DATE_FORMAT);
        }

        return null;
    }

    private class DateValueFormatter extends ValueFormatter {
        private final DateFormat dateFormat;

        DateValueFormatter(DateFormat dateFormat) {
            super();
            this.dateFormat = dateFormat;
        }

        @Override
        public String getFormattedValue(float val) {

            long actualTimeStamp =
                    (((long) val * SMALL_UNIX_TIME_SCALAR) + UNIX_TIME_2019) * UNIX_TIME_SCALAR;
            Date actualDate = new Date(actualTimeStamp);
            return dateFormat.format(actualDate);
        }
    }
}


