package com.example.weatherapp.managers;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;

public class WeatherServiceImpl implements WeatherService {
    private static final String BASE_URL = "https://prod-04.centralus.logic.azure.com/workflows/a" +
            "341276fb5af473f90dcd69d2e5877cb/triggers/manual/paths/invoke/";
    private static final String BASE_URL_END = "/?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%" +
            "2Frun&sv=1.0&sig=7GfqW_95qdoSm2VAiW2ZSSXyNHo9Ie7yBH9xVKGXtBM";
    private static final String THRESHOLD_BASE_URL =
            "http://ec2-3-18-106-200.us-east-2.compute.amazonaws.com:3000/resources/";
    private static final MediaType TEXT_PLAIN_MEDIA_TYPE = MediaType.parse("text/plain");

    @SuppressWarnings("WeakerAccess") //Dagger
    @Inject
    public WeatherServiceImpl() {
        NetModule netModule = new NetModule(BASE_URL);
        NetModule_ProvideOkhttpClientFactory okhttpClientFactory
                = new NetModule_ProvideOkhttpClientFactory(netModule);
        NetModule_ProvideGsonFactory gsonFactory
                = new NetModule_ProvideGsonFactory(netModule);
        NetModule_ProvideRetrofitFactory retrofitFactory
                = new NetModule_ProvideRetrofitFactory(netModule, gsonFactory, okhttpClientFactory);
        WeatherServiceImpl_MembersInjector.create(
                okhttpClientFactory,
                retrofitFactory,
                gsonFactory
        ).injectMembers(this);
    }

    @SuppressWarnings("WeakerAccess") //Dagger
    @Inject
    public OkHttpClient okHttpClient;

    @SuppressWarnings("WeakerAccess") //Dagger
    @Inject
    public Retrofit retrofit;

    @SuppressWarnings("WeakerAccess") //Dagger
    @Inject
    public Gson gson;

    @Override
    public void getDataPoints(
            final WeatherType weatherType,
            final TimeScale timeScale,
            final Consumer<List<Pair<String, Double>>> consumer
    ) {
        final Calendar calendar = Calendar.getInstance(Locale.US);

        final Date startDate;
        final Date endDate = calendar.getTime();
        switch (timeScale) {
            case ONE_DAY:
                calendar.add(Calendar.DAY_OF_MONTH, -1);
                startDate = calendar.getTime();
                break;
            case ONE_WEEK:
                calendar.add(Calendar.DAY_OF_MONTH, -7);
                startDate = calendar.getTime();
                break;
            case ONE_MONTH:
                calendar.add(Calendar.MONTH, -1);
                startDate = calendar.getTime();
                break;
            default:
                startDate = calendar.getTime();
        }

        SimpleDateFormat brandonDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String startDateString = brandonDateFormat.format(startDate);
        String endDateString = brandonDateFormat.format(endDate);
        String resource = weatherType.getApiResourceString();


        String url = BASE_URL +
                startDateString + '/' +
                endDateString + '/' +
                resource +
                BASE_URL_END;

        Request request = new Request.Builder()
                .url(url)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(
                    @NonNull final Call call,
                    @NonNull final IOException e
            ) {
                Log.e("HTTP", "Request failed");
            }

            @Override
            public void onResponse(
                    @NonNull final Call call,
                    @NonNull final Response response
            ) throws IOException {
                String body = Objects.requireNonNull(
                        response.body(),
                        "Response had no body."
                ).string();

                if (body != null && !body.isEmpty()) {

                    List<Pair<String, Double>> parsedList = processResponse(body);

                    consumer.accept(parsedList);
                } else {
                    Log.i("HTTP", "Got empty body back.");
                }
            }
        });
    }

    @Override
    public void setMinThreshold(
            final WeatherType weatherType,
            final float lowerBound,
            final Runnable successRunnable,
            final Runnable errorRunnable
    ) {

        String resource = weatherType.getThresholdString();

        if (resource != null) {
            String baseUrl = THRESHOLD_BASE_URL +
                    resource + '/';

            String minUrl = baseUrl + "min";

            Request.Builder minRequestBuilder = new Request.Builder()
                    .url(minUrl);

            setThreshold(minRequestBuilder, lowerBound, successRunnable, errorRunnable);
        } else {
            Log.e("CODE", "Invalid weather type for threshold.");
        }
    }

    @Override
    public void setMaxThreshold(
            WeatherType weatherType,
            float upperBound,
            Runnable successRunnable,
            Runnable errorRunnable
    ) {
        String resource = weatherType.getThresholdString();

        if (resource != null) {
            String baseUrl = THRESHOLD_BASE_URL +
                    resource + '/';

            String maxUrl = baseUrl + "max";

            Request.Builder maxRequestBuilder = new Request.Builder()
                    .url(maxUrl);

            setThreshold(maxRequestBuilder, upperBound, successRunnable, errorRunnable);
        } else {
            Log.e("CODE", "Invalid weather type for threshold.");
        }
    }

    private void setThreshold(
            Request.Builder requestBuilder,
            float threshold,
            Runnable successRunnable,
            Runnable errorRunnable
    ) {
        requestBuilder.post(
                RequestBody.create(TEXT_PLAIN_MEDIA_TYPE, Float.toString(threshold))
        );

        Callback callback = new Callback() {
            @Override
            public void onFailure(
                    @NonNull final Call call,
                    @NonNull final IOException e
            ) {
                Log.e("HTTP", "Request failed");
                try {
                    errorRunnable.run();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onResponse(
                    @NonNull final Call call,
                    @NonNull final Response response
            ) {
                try {
                    successRunnable.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        okHttpClient.newCall(requestBuilder.build()).enqueue(callback);
    }


    private List<Pair<String, Double>> processResponse(String body) {
        boolean requestSucceeded = true;

        if (body.isEmpty()) {
            Log.e("HTTP", "Got back response with empty body.");
            requestSucceeded = false;
        }

        if (body.contains("\"error\":")) {
            Log.e("HTTP", "Message body contained error. Body: " + body);
            requestSucceeded = false;
        }

        List<Pair<String, Double>> dataPoints = null;
        if (requestSucceeded) {
            dataPoints = Arrays.stream(gson.fromJson(body, WeatherItem[].class))
                    .map(WeatherItem::convertToDataPoint)
                    .collect(Collectors.toList());
        }

        return dataPoints;
    }
}
