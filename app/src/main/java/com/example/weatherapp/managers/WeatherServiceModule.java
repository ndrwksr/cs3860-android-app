package com.example.weatherapp.managers;

import dagger.Module;
import dagger.Provides;

@Module
public class WeatherServiceModule {
    @Provides
    WeatherService getWeatherService() {
        return new WeatherServiceImpl();
    }
}
