package com.example.weatherapp.managers;

public enum TimeScale {
    ONE_DAY("day", "Daily"),
    ONE_WEEK("week", "Weekly"),
    ONE_MONTH("month", "Monthly");

    private String apiArgString;
    private String prettyString;

    TimeScale(String apiArgString, String prettyString) {
        this.apiArgString = apiArgString;
        this.prettyString = prettyString;
    }

    public String getApiArgString() {
        return apiArgString;
    }

    public String getPrettyString() {
        return prettyString;
    }

    public static TimeScale fromApiArgString(String apiArgString) {
        TimeScale timeScale = null;
        for (TimeScale loopTimeScale : TimeScale.values()) {
            if (apiArgString.equals(loopTimeScale.getApiArgString())) {
                timeScale = loopTimeScale;
            }
        }
        return timeScale;
    }

    public static TimeScale fromPrettyString(String prettyString) {
        TimeScale timeScale = null;
        for (TimeScale loopTimeScale : TimeScale.values()) {
            if (prettyString.equals(loopTimeScale.getPrettyString())) {
                timeScale = loopTimeScale;
            }
        }
        return timeScale;
    }
}
