package com.example.weatherapp.managers;

import android.util.Pair;

import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.List;

public interface GraphManager {
    BarDataSet getBarDataSet(
            List<Pair<String, Double>> rawData,
            String label
    ) throws IllegalArgumentException;

    ValueFormatter getValueFormatter(
            TimeScale timeScale
    );

    LineDataSet getLineDataSet(
            List<Pair<String, Double>> rawData,
            String label
    );
}
