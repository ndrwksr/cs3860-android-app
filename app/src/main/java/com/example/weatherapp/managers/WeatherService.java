package com.example.weatherapp.managers;

import android.util.Pair;

import com.jjoe64.graphview.series.DataPoint;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import javax.inject.Singleton;

import dagger.Component;

public interface WeatherService {
    void getDataPoints(
            WeatherType weatherType,
            TimeScale timeScale,
            Consumer<List<Pair<String, Double>>> consumer
    );

    void setMinThreshold(
            final WeatherType weatherType,
            final float lowerBound,
            final Runnable successRunnable,
            final Runnable errorRunnable
    );

    void setMaxThreshold(
            final WeatherType weatherType,
            final float upperBound,
            final Runnable successRunnable,
            final Runnable errorRunnable
    );
}
