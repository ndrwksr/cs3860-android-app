package com.example.weatherapp.managers;

import dagger.Component;

@Component(modules = {WeatherServiceModule.class}, dependencies = {NetComponent.class})
public interface WeatherServiceComponent {
    WeatherService getWeatherService();
}
