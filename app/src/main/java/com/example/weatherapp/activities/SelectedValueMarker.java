package com.example.weatherapp.activities;

import android.content.Context;
import android.widget.TextView;

import com.example.weatherapp.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.util.Locale;

public class SelectedValueMarker extends MarkerView {

    private TextView tvContent;
    private final String unit;

    public SelectedValueMarker(Context context, int layoutResource, String unit) {
        super(context, layoutResource);
        this.unit = unit;
        // this markerview only displays a textview
        tvContent = findViewById(R.id.selectedMarkerTextview);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        tvContent.setText(String.format(Locale.US, "%.2f%s", e.getY(), unit)); // set the entry-value as the display text
        super.refreshContent(e, highlight);
    }
}