package com.example.weatherapp.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Pair;

import com.example.weatherapp.R;
import com.example.weatherapp.managers.WeatherType;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.List;
import java.util.Locale;

public class TemperatureActivity extends GraphActivity {
    static final String ACTIVITY_NAME = "Temperature";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_temp);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getActivityName() {
        return ACTIVITY_NAME;
    }

    @Override
    protected DataSet applyStyleToDataSet(DataSet dataSet) {
        dataSet.setGradientColor(
                Color.parseColor("#1CD100"),
                Color.parseColor("#F2F203")
        );
        return dataSet;
    }

    @Override
    protected void styleChart(DataSet dataSet) {
        BarChart chart = (BarChart) getChart();
        chart.setScaleEnabled(false);
        chart.getAxisLeft().setAxisMaximum(100f);
        chart.getAxisLeft().setAxisMinimum(0f);
        chart.getAxisLeft().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.format(Locale.US, "%.2f", value) + "°C";
            }
        });
        chart.getAxisRight().setEnabled(false);
        chart.getDescription().setEnabled(false);

        IMarker marker = new SelectedValueMarker(this, R.layout.selected_marker_textview, "°C");
        chart.setMarker(marker);

        chart.setMaxVisibleValueCount(dataSet.getEntryCount());

        chart.setDrawBarShadow(false);
        chart.setDrawGridBackground(false);

        chart.setFitBars(true);
    }

    @Override
    protected void applyDataSet(DataSet dataSet) {
        BarData barData = new BarData((BarDataSet) dataSet);
        barData.setBarWidth(20f);
        //noinspection RedundantCast
        ((BarChart) getChart()).setData(barData);
    }

    @Override
    protected DataSet getDataSet(List<Pair<String, Double>> rawData) {
        return graphManager.getBarDataSet(
                rawData,
                getActivityName()
        );
    }

    protected WeatherType getWeatherType() {
        return WeatherType.TEMPERATURE;
    }
}
