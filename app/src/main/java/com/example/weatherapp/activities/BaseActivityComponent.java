package com.example.weatherapp.activities;

import com.example.weatherapp.managers.GraphManagerModule;
import com.example.weatherapp.managers.WeatherServiceModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Component(modules = {BaseActivityModule.class}, dependencies = {WeatherApplicationComponent.class})
public interface BaseActivityComponent extends AndroidInjector<BaseActivity> {
}
