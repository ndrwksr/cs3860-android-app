package com.example.weatherapp.activities;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Module
public abstract class WeatherApplicationModule {
    @ContributesAndroidInjector
    abstract WeatherApplication contributeActivityInjector();
}
