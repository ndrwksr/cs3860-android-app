package com.example.weatherapp.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Pair;

import com.example.weatherapp.R;
import com.example.weatherapp.managers.WeatherType;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.List;

public class WindSpeedActivity extends GraphActivity {

    public static final String ACTIVITY_NAME = "Wind Speed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_windspeed);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getActivityName() {
        return ACTIVITY_NAME;
    }

    @Override
    protected WeatherType getWeatherType() {
        return WeatherType.WIND_SPEED;
    }

    @Override
    protected DataSet getDataSet(List<Pair<String, Double>> rawData) {
        return graphManager.getLineDataSet(
                rawData,
                getActivityName()
        );
    }

    @Override
    protected DataSet applyStyleToDataSet(DataSet dataSet) {
        dataSet.setColor(Color.GREEN);
        return dataSet;
    }

    @Override
    protected void styleChart(DataSet dataSet) {
        LineChart chart = (LineChart) getChart();
        chart.getAxisLeft().setAxisMinimum(0f);
        chart.getAxisRight().setEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.setMaxVisibleValueCount(dataSet.getEntryCount());
        chart.setDrawGridBackground(false);
    }

    @Override
    protected void applyDataSet(DataSet dataSet) {
        LineData lineData = new LineData((LineDataSet) dataSet);
        //noinspection RedundantCast
        ((LineChart) getChart()).setData(lineData);
    }
}
