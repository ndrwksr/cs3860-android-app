package com.example.weatherapp.activities;

import android.app.Application;

import com.example.weatherapp.managers.GraphManager;
import com.example.weatherapp.managers.GraphManagerModule;
import com.example.weatherapp.managers.NetModule;
import com.example.weatherapp.managers.WeatherService;
import com.example.weatherapp.managers.WeatherServiceModule;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Component(modules = {
        AndroidInjectionModule.class,
        WeatherApplicationModule.class,
        BaseActivityModule.class,
        GraphManagerModule.class,
        WeatherServiceModule.class
})
public interface WeatherApplicationComponent extends AndroidInjector<WeatherApplication> {
    GraphManager graphManager();
    WeatherService weatherService();
}