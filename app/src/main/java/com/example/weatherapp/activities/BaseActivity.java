package com.example.weatherapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.weatherapp.R;
import com.example.weatherapp.managers.GraphManager;
import com.example.weatherapp.managers.WeatherService;
import com.github.mikephil.charting.charts.Chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public abstract class BaseActivity extends AppCompatActivity {
    private final List<String> activityTitles = Arrays.asList(
            RainfallActivity.ACTIVITY_NAME,
            WindSpeedActivity.ACTIVITY_NAME,
            TemperatureActivity.ACTIVITY_NAME,
            HumidityActivity.ACTIVITY_NAME,
            ThresholdActivity.ACTIVITY_NAME
    );

    @Inject
    public GraphManager graphManager;

    @Inject
    public WeatherService weatherService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        initializeDrawer();
    }

    protected ListView getNavListView() {
        return findViewById(R.id.navList);
    }

    protected Spinner getTimeSpinner() {
        return findViewById(R.id.timeSpinner);

    }

    protected Chart getChart() {
        return findViewById(R.id.chart);
    }

    protected abstract String getActivityName();

    protected void initializeDrawer() {
        final String activityTitle = getActivityName();
        ListView navListView = getNavListView();

        //This contains the possible navigation destinations. If we were given a navigation key,
        //we remove it from the list so that the user isn't given the option to navigate to where
        //they already are.
        final List<String> destinationList = new ArrayList<>(activityTitles);
        destinationList.remove(activityTitle);

        //Create an adapter using the navigation destinations, and navigate based on which item
        //was selected.
        ArrayAdapter<String> drawerDestinations =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, destinationList);
        navListView.setAdapter(drawerDestinations);
        navListView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = null;
            switch (destinationList.get(position)) {
                case RainfallActivity.ACTIVITY_NAME:
                    intent = new Intent(getApplicationContext(), RainfallActivity.class);
                    break;
                case WindSpeedActivity.ACTIVITY_NAME:
                    intent = new Intent(getApplicationContext(), WindSpeedActivity.class);
                    break;
                case TemperatureActivity.ACTIVITY_NAME:
                    intent = new Intent(getApplicationContext(), TemperatureActivity.class);
                    break;
                case HumidityActivity.ACTIVITY_NAME:
                    intent = new Intent(getApplicationContext(), HumidityActivity.class);
                    break;
                case ThresholdActivity.ACTIVITY_NAME:
                    intent = new Intent(getApplicationContext(), ThresholdActivity.class);
                default:
                    break;
            }
            if (intent != null) {
                startActivity(intent);
            } else {
                throw new RuntimeException("Unable to create navigation intent.");
            }
        });
    }




}
