package com.example.weatherapp.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Pair;

import com.example.weatherapp.R;
import com.example.weatherapp.managers.WeatherType;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.List;
import java.util.Locale;

public class HumidityActivity extends GraphActivity {

    public static final String ACTIVITY_NAME = "Humidity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_humidity);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getActivityName() {
        return ACTIVITY_NAME;
    }

    @Override
    protected WeatherType getWeatherType() {
        return WeatherType.HUMIDITY;
    }

    @Override
    protected DataSet getDataSet(List<Pair<String, Double>> rawData) {
        return graphManager.getLineDataSet(
                rawData,
                getActivityName()
        );
    }

    @Override
    protected DataSet applyStyleToDataSet(DataSet dataSet) {
        dataSet.setColor(Color.GREEN);
        return dataSet;
    }

    @Override
    protected void styleChart(DataSet dataSet) {
        LineChart chart = (LineChart) getChart();
        chart.setScaleEnabled(false);
        chart.getAxisLeft().setAxisMaximum(100f);
        chart.getAxisLeft().setAxisMinimum(0f);
        chart.getAxisLeft().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.format( Locale.US, "%.2f", value) + "%";
            }
        });
        chart.getAxisRight().setEnabled(false);
        chart.getDescription().setEnabled(false);

        IMarker marker = new SelectedValueMarker(this, R.layout.selected_marker_textview, "°C");
        chart.setMarker(marker);

        chart.setMaxVisibleValueCount(dataSet.getEntryCount());

        chart.setDrawGridBackground(false);
    }

    @Override
    protected void applyDataSet(DataSet dataSet) {
        LineData lineData = new LineData((LineDataSet) dataSet);
        //noinspection RedundantCast
        ((LineChart) getChart()).setData(lineData);
    }
}
