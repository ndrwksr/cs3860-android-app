package com.example.weatherapp.activities;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BaseActivityModule {
    @ContributesAndroidInjector
    abstract BaseActivity contributeBaseActivityInjector();

    @ContributesAndroidInjector
    abstract RainfallActivity contributesRainfallActivityInjector();

    @ContributesAndroidInjector
    abstract WindSpeedActivity contributesWindSpeedActivityInjector();

    @ContributesAndroidInjector
    abstract TemperatureActivity contributesTempActivityInjector();

    @ContributesAndroidInjector
    abstract HumidityActivity contributesHumidityActivityInjector();

    @ContributesAndroidInjector
    abstract ThresholdActivity contributesThresholdActivityInjector();
}
