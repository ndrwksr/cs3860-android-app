package com.example.weatherapp.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.weatherapp.R;
import com.example.weatherapp.managers.WeatherType;

public class ThresholdActivity extends BaseActivity {
    public static final String ACTIVITY_NAME = "Thresholds";
    private static final float TEMP_MIN = 0f;
    private static final float TEMP_MAX = 30f;

    private static final float WIND_MIN = 0f;
    private static final float WIND_MAX = 50f;


    SeekBar tempMinSeekbar;
    SeekBar tempMaxSeekbar;
    SeekBar windMinSeekbar;
    SeekBar windMaxSeekbar;

    private float tempMin;
    private float tempMax;
    private float windMin;
    private float windMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_threshold);
        super.onCreate(savedInstanceState);

        tempMinSeekbar = findViewById(R.id.tempMinSeekbar);
        tempMaxSeekbar = findViewById(R.id.tempMaxSeekbar);
        windMinSeekbar = findViewById(R.id.windMinSeekbar);
        windMaxSeekbar = findViewById(R.id.windMaxSeekbar);

        tempMaxSeekbar.setProgress(100);
        windMaxSeekbar.setProgress(100);

        tempMinSeekbar.setOnSeekBarChangeListener(
                new ThresholdSeekBarChangeListener(
                        true,
                        TEMP_MAX,
                        TEMP_MIN,
                        WeatherType.TEMPERATURE
                )
        );
        tempMaxSeekbar.setOnSeekBarChangeListener(
                new ThresholdSeekBarChangeListener(
                        false,
                        TEMP_MAX,
                        TEMP_MIN,
                        WeatherType.TEMPERATURE
                )
        );
        windMinSeekbar.setOnSeekBarChangeListener(
                new ThresholdSeekBarChangeListener(
                        true,
                        WIND_MAX,
                        WIND_MIN,
                        WeatherType.WIND_SPEED
                )
        );
        windMaxSeekbar.setOnSeekBarChangeListener(
                new ThresholdSeekBarChangeListener(
                        false,
                        WIND_MAX,
                        WIND_MIN,
                        WeatherType.WIND_SPEED
                )
        );
    }

    @Override
    protected String getActivityName() {
        return ACTIVITY_NAME;
    }

    private class ThresholdSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {
        private final boolean isMin;
        private final float maxVal;
        private final float minVal;
        private final WeatherType weatherType;

        private ThresholdSeekBarChangeListener(
                boolean isMin,
                float thresholdMax,
                float thresholdMin,
                WeatherType weatherType
        ) {
            this.isMin = isMin;
            this.maxVal = thresholdMax;
            this.minVal = thresholdMin;
            this.weatherType = weatherType;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            seekBar.setBackgroundColor(Color.BLUE);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            float value = (maxVal - minVal) * (((float) seekBar.getProgress()) / 100) + minVal;

            boolean isValid;
            switch (weatherType) {
                case TEMPERATURE:
                    isValid = tempMinSeekbar.getProgress() <= tempMaxSeekbar.getProgress();
                    break;
                case WIND_SPEED:
                    isValid = windMinSeekbar.getProgress() <= windMaxSeekbar.getProgress();
                    break;
                default:
                    throw new IllegalArgumentException("weatherType was invalid.");
            }

            if (isValid) {
                setThresholdLabel(weatherType, isMin, value);
                invokeWeatherService(weatherType, seekBar, isMin, value);
            } else {
                if (isMin) {
                    seekBar.setProgress(0);
                } else {
                    seekBar.setProgress(100);
                }
                seekBar.setBackgroundColor(Color.RED);
            }
        }
    }

    private void invokeWeatherService(
            WeatherType weatherType,
            SeekBar seekBar,
            boolean isMin,
            float value
    ) {
        if (isMin) {
            weatherService.setMinThreshold(
                    weatherType,
                    value,
                    () -> seekBar.setBackgroundColor(Color.GREEN),
                    () -> seekBar.setBackgroundColor(Color.RED)
            );
        } else {
            weatherService.setMaxThreshold(
                    weatherType,
                    value,
                    () -> seekBar.setBackgroundColor(Color.GREEN),
                    () -> seekBar.setBackgroundColor(Color.RED)
            );
        }
    }

    private void setThresholdLabel(WeatherType weatherType, boolean isMin, float value) {
        TextView textView;
        String thresholdString;
        switch (weatherType) {
            case TEMPERATURE:
                if (isMin) {
                    tempMin = value;
                } else {
                    tempMax = value;
                }
                textView = findViewById(R.id.tempThreshold);
                thresholdString = Float.toString(tempMin) + " - " + Float.toString(tempMax) + "C";
                break;
            case WIND_SPEED:
                if (isMin) {
                    windMin = value;
                } else {
                    windMax = value;
                }
                textView = findViewById(R.id.windThreshold);
                thresholdString = Float.toString(windMin) + " - " + Float.toString(windMax) + "m/s";
                break;
            default:
                throw new IllegalArgumentException("weatherType must be TEMPERATURE or WIND_SPEED");
        }
        textView.setText(thresholdString);
    }
}