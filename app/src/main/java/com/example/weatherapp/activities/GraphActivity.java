package com.example.weatherapp.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.weatherapp.R;
import com.example.weatherapp.managers.TimeScale;
import com.example.weatherapp.managers.WeatherType;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.Arrays;
import java.util.List;

public abstract class GraphActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializePullToRefresh();
        initializeControls();
    }

    protected abstract WeatherType getWeatherType();

    protected abstract DataSet getDataSet(List<Pair<String, Double>> rawData);

    protected abstract DataSet applyStyleToDataSet(DataSet dataSet);

    protected abstract void styleChart(DataSet dataSet);

    protected abstract void applyDataSet(DataSet dataSet);

    private void initializePullToRefresh() {
        SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(() -> {
            refresh();
            pullToRefresh.setRefreshing(false);
        });
    }

    private void initializeControls() {
        Spinner timeSpinner = getTimeSpinner();

        timeSpinner.setAdapter(new ArrayAdapter<>(
                this,
                R.layout.spinner_textview,
                Arrays.stream(TimeScale.values()).map(TimeScale::getPrettyString).toArray())
        );

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                refresh();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //NOOP
            }
        });
    }

    private void refresh() {
        TimeScale timeScale = TimeScale.fromPrettyString(getTimeSpinner().getSelectedItem().toString());
        final BaseActivity finalThis = this;
        weatherService.getDataPoints(getWeatherType(), timeScale,
                pairs -> finalThis.runOnUiThread(() -> {
                    if (pairs == null)
                        return;
                    DataSet dataSet = getDataSet(pairs);
                    if (dataSet == null)
                        return;

                    Chart chart = getChart();
                    XAxis xAxis = chart.getXAxis();
                    ValueFormatter valueFormatter = graphManager.getValueFormatter(timeScale);
                    xAxis.setValueFormatter(valueFormatter);
                    xAxis.setSpaceMin(25f);
                    xAxis.setSpaceMax(25f);
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    xAxis.setDrawGridLines(false);

                    DataSet styledDataSet = applyStyleToDataSet(dataSet);

                    styleChart(styledDataSet);
                    applyDataSet(styledDataSet);

                    chart.invalidate();
                })
        );
    }
}
